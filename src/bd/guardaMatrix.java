/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd;
import bd.components.FileChooserDemo2;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.table.TableModel;
/**
 *
 * @author Uriel
 */
public class guardaMatrix {
    TableModel tabla;
    String[][] matrix;
    FileOutputStream fos;
    DataOutputStream salida;
    String nombre;
    String cabecera[];
    public guardaMatrix(String nombre, TableModel tabla, String cabecera[]){
        this.tabla = tabla;
        this.nombre = nombre;
        this.fos = null;
        this.salida = null;
        this.cabecera = cabecera;
    }
    public void imprimeMatrix(){
        for(int i=0;i<tabla.getRowCount();i++){
            for(int j=0;j<tabla.getRowCount();j++){
                System.out.println(tabla.getValueAt(i, j));
            }
        }
    }
    public void guardaM(){
        matrix = new String[tabla.getRowCount()][tabla.getColumnCount()];
        //Guardando en el fichero binario
        try{
            fos = new FileOutputStream("src/bd/tablas/"+this.nombre+".tabla");
            salida = new DataOutputStream(fos);
            salida.writeInt(tabla.getRowCount());
            salida.writeInt(tabla.getColumnCount());
            nombre+='\n';
            salida.writeChars(nombre);
            for(int i=0;i<tabla.getColumnCount();i++){
                salida.writeChars(cabecera[i]);
                salida.writeChar('\n');
            }
            for(int i=0;i<tabla.getRowCount();i++){
                for(int j=0;j<tabla.getColumnCount();j++){
                    if(String.valueOf(tabla.getValueAt(i,j)).isEmpty())
                        matrix[i][j]="null\n";//para indicar que esa casilla esta vacia al momento de leerla
                    matrix[i][j] = String.valueOf(tabla.getValueAt(i,j));//retornar n
                    matrix[i][j]+='\n';
                    salida.writeChars(matrix[i][j]);
                    System.out.println(matrix[i][j]);
                }
            }        
            salida.close();
            fos.close();
            System.out.println("LISTO");
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e.getMessage() 
                , "ERROR", JOptionPane.ERROR_MESSAGE);
            }
         
    }
}
