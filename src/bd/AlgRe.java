/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.TableModel;

/**
 *
 * @author Uriel
 */
public class AlgRe {
    ArrayList<String> headers;
    int filas1,filas2;
    int columnas1,columnas2;
    String nombre1, nombre2, path;
    int opcion;
    ArrayList<String> cabeceras1;
    ArrayList<String> cabeceras2;
    String query1, query2="";
    String matrix[][];
    String matrix2[][];//Matrix para la segunda tabla(opcional)
    String matAux[][];
    int filas, columnas;
    boolean matrixVerifica[];
    abreMatrix aMatrix;
    Analizador analiza;
    boolean exito=false;
    boolean tuplas[];
    boolean tuplas2[];
    boolean tuplas3[];
    int valor;
    ArrayList<String> lista;//lista se vaciara en matAux
    TableModel tabla;
    public AlgRe(String query){
        query1=query;
    }
    public AlgRe(String query1, String query2, int opcion){
        this.query1=query1;
        this.query2=query2;
        this.opcion = opcion;
    }
    public void getTabla(){
        if(generaMatrix(query1)){
            System.out.println("FILAS "+getFilas());
            System.out.println("COLUMNAS "+getColumnas());
            System.out.println("CABECERAS======= ");
            while(true){
                if(headers.size()>0){
                    System.out.println(headers.remove(0));
                }else{
                    break;
                }
            }
        }
    }
    public ArrayList getHeaders(){
        return this.headers;
    }
    public int getFilas(){
        return this.filas;
    }
    public int getColumnas(){
        return this.columnas;
    }
    public String[] getCabeceras(){
        return this.aMatrix.getHeader();
    }
    public String getValorMatrix(int i,int j){
        return this.matAux[i][j];
    }
    public boolean generaMatrix(String q){
        analiza = new Analizador(q);
        analiza.descompone();
        JOptionPane.showMessageDialog(null, "valor de la tabla"+analiza.tabla);
        String aux[];
        int f1,f2;
        int contador=0;
        if(analiza.tabla.contains(";"))analiza.tabla = analiza.tabla.replace(";", ""); 
        path = String.valueOf(System.getProperty("user.dir"));
        System.out.println("PATH///////// "+analiza.getNombreTabla());
        path += "\\src\\bd\\tablas\\"+analiza.getNombreTabla()+".tabla";
        File fichero = new File(path);
        System.out.println("PATH "+path);
        //si el fichero existe
        if(fichero.isFile()){
            exito = true;
            aMatrix = new abreMatrix(path);
            aMatrix.cargaMatrix();
            filas1 = aMatrix.getFilas();
            columnas1 = aMatrix.getColumnas();
            //String campAux[];
            //System.out.println(analiza.getCampoValidacion()+"validacion");
            aux = analiza.getStringCampos();
            //Inicializamos matrixVerifica, que nos ayudara a mostrar los campos select y where del query, pasandolos al final a la nueva matrix.
            matrixVerifica = new boolean[aMatrix.columnas];
            //formatear array de filas 
            for(int i=0;i<aMatrix.columnas;i++) matrixVerifica[i]=false;
            //System.out.println("campo "+aux[0]);
            //Seleccionar campos del select(!ESTOS CAMPOS!) from...
            //Si dentro del select() hay * selecciona todos los campos
            if(analiza.getStringCampos().length==1&&"*".equals(aux[0])){
                headers = new ArrayList();
                for(int i=0;i<aMatrix.columnas;i++){
                    matrixVerifica[i]=true;  
                    headers.add(aMatrix.cabecera[i]);
                }
                      //System.out.println(exito+"1");
            }else{//Si son campos especificos se verificara que existan esos campos de analiza.campos
                int cuenta=0;
                //Analizar si todos los campos que descubrio el analizador existen en la matriz
                    headers = new ArrayList();
                    /*for(int i = 0;i<analiza.campos.length;i++){
                        JOptionPane.showMessageDialog(null, analiza.campos[i]);
                    }*/
                    for(int i = 0;i<analiza.campos.length;i++){
                        for(int j=0;j<aMatrix.cabecera.length;j++){
                            //System.out.println("analiza "+analiza.campos[i]+" matrix "+aMatrix.cabecera[j]);
                            //JOptionPane.showMessageDialog(null, "cabecera"+aMatrix.cabecera[j]+"equals"+analiza.campos[i]);
                            if(analiza.campos[i].equals(aMatrix.cabecera[j])){
                                cuenta++;
                                matrixVerifica[j]=true;
                                headers.add(aMatrix.cabecera[j]);
                                System.out.println("!!!Nombre cabecera "+aMatrix.cabecera[j]+"indice "+j);
                                exito = true;
                                //break;
                            }/*else{
                                //matrixVerifica[0][j]=false;
                                //exito = false;
                            }*/
                        }
                    }
                    //System.out.println("analiza "+analiza.campos.length+" cuenta "+cuenta);
                    //Se asegura que la cantidad de campos del query sea igual al numero de nombre de columnas encontrados
                    //if(analiza.campos.length!=cuenta) exito=false;
                    //System.out.println(exito+";)");
                }
                //for(int i=0;i<aMatrix.columnas;i++) System.out.println("MATRIX i"+i+"valor" +matrixVerifica[0][i]);
                System.out.println("exitoSSSS ==== "+exito);
                if(exito){
                    //Si existe where
                    //System.out.println("1");
                    tuplas = new boolean[aMatrix.filas];//Regresara un array booleano sobre el cual nos ayudaremos para imprimir las tuplas
                    //formatear tuplas con valor inicial false
                    for(int i=0;i<aMatrix.filas;i++) tuplas[i]=false;
                    int numeroDeCampo=-1;//Es el indice de la columna donde esta CampoValidacion
                    if(null!=analiza.getValorCampo()&&analiza.getCampoValidacion()!=null){//verifica que exista una clausula where campoValidacion=valorCampo
                        //Marcar en la matrix cuales tuplas se mostraran, se valida el where campo = valor
                        //Este ciclo solo verifica que el campoValidacion es un header valido de la tabla 
                        for(int i=0;i<aMatrix.columnas;i++){
                            if(aMatrix.cabecera[i].equals(analiza.getCampoValidacion())){
                                numeroDeCampo = i;
                                setValor(i);
                                break;
                            }
                        }
                        if(numeroDeCampo!=-1){
                            for(int i=0;i<aMatrix.filas;i++){
                                if(analiza.getValorCampo().equals(aMatrix.getValor(i, numeroDeCampo))){
                                    tuplas[i]=true;
                                    System.out.println("TUPLA en "+i);
                                }
                            }
                        }else{
                            exito = false;
                        }//Ahora a guardar las tuplas del tamaño que deben ser, para pasar a mostrarlas
                        //System.out.println("2");
                        /*if(false){
                            int rows=0,col=0;
                            //creacion de la tabla de dimension nxm donde n=tuplas generadas, m=campos del select(!estos campos!) from...
                            for(int i=0;i<tuplas.length;i++){
                                if(tuplas[i]=true){
                                    rows++;
                                }
                            }
                            for(int i=0;i<aMatrix.columnas;i++){
                                if(matrixVerifica[0][i]==true){
                                    System.out.println("valor de i"+i);
                                    col++;
                                }
                            }
                            if(rows>0&&col>0) exito = true;
                            if(exito){
                                int k=0,h=0;
                                //System.out.println("===========================!!!!!!!!!!");
                                //System.out.println(aMatrix.getValor(0, 0));
                                matAux = new String[rows][col];
                                for(int i=0;i<aMatrix.filas;i++){
                                    for(int j=0;j<aMatrix.columnas;j++){
                                        if(matrixVerifica[0][j]==true&&tuplas[j]==true){
                                            System.out.println("valor de j"+j);
                                            matAux[k][h] = aMatrix.getValor(i, j);
                                            h++;
                                        }
                                    }
                                    h=0;
                                }
                                this.filas = rows;
                                this.columnas = col;
                                /*System.out.println("entra ==== ");
                                int k=0,h=0;
                                campAux = analiza.getStringCampos();
                                boolean colAux[] = new boolean[aMatrix.columnas];
                                for(int i=0;i<analiza.campos.length;i++){
                                    for(int j=0;j<aMatrix.columnas;j++){
                                        if(campAux[i].equals(aMatrix.getValor(i, j))){
                                            colAux[i]=true;
                                        }else{
                                            colAux[i]=false;
                                        }
                                    }
                                }
                                matAux = new String[rows][col];
                                for(int i=0;i<tuplas.length;i++){
                                    for(int j=0;j<aMatrix.columnas;j++){
                                        if(colAux[j]==true&&tuplas[i]==true){
                                            matAux[k][h]=aMatrix.getValor(i, j);
                                            System.out.println("HOLAAAA "+aMatrix.getValor(i, j));
                                            h++;
                                        }
                                        
                                    }
                                    h=0;
                                    k++;
                                }*/
                            /*}
                            for(int i =0;i<rows;i++){
                                for(int j=0; j<col;j++){
                                    System.out.println("MatrixXXX"+matAux[i][j]);
                                }
                            }
                        }*/
                    }else{//Si no hay where
                        System.out.println("no hay where");
                        tuplas = new boolean[aMatrix.filas];
                        for(int i=0;i<aMatrix.filas;i++) tuplas[i]=true;
                    }//Si hay where
                }
                //Asignar las tablas teniendo ya las tuplas y columnas que se mostraran
                if(exito){
                    int n1=0,n2=0;
                    for(int i=0;i<aMatrix.filas;i++){
                        if(tuplas[i]==true) n1++;
                    }
                    for(int j=0;j<aMatrix.columnas;j++){
                        if(matrixVerifica[j]==true) n2++;
                    }
                    System.out.println("n1 "+n1+" n2 "+n2);
                    //Se crea la nueva tabla de dimension n1 X n2
                    matAux = new String[n1][n2];
                    int h=0,k=0;
                    lista = new ArrayList();
                    for(int i=0;i<aMatrix.filas;i++){
                        for(int j=0;j<aMatrix.columnas;j++){
                            if(tuplas[i]==true&&matrixVerifica[j]==true){
                                System.out.println("Matrix! "+aMatrix.getValor(i, j)+" h "+h+" k "+k);
                                lista.add(aMatrix.getValor(i, j));
                                //matAux[h][k]=aMatrix.getValor(i, j);
                                //k++;
                            }
                        }
                        //k=0;
                        //h++;
                    }
                    this.filas = n1;
                    this.columnas = n2;
                    for(int i=0;i<n1;i++){
                        for(int j=0;j<n2;j++){
                            try{
                            matAux[i][j]=lista.remove(0);
                            System.out.println("Matrix en n1 "+i+" n2 "+j+" VALOR "+matAux[i][j]);
                            }catch(Exception e){ exito=false;
                            System.out.println("ERROR "+e.getMessage());}
                        }
                    }
                }
                
                
             

             
             
             //System.out.println("Existe");
        }
        else{
            //System.out.println("No existe");
            exito=false;
        }
        return exito;
    }
    public void setValor(int i){
        this.valor =i;
    }
    public boolean unionTablas(){
        boolean bandera=true;
        int f1,f2,c1,c2;
        ArrayList<String> cabeceras1=new ArrayList();
        ArrayList<String> cabeceras2=new ArrayList();
        if(!"".equals(query2)){
            if(generaMatrix(query1)){
                f1 = getFilas();
                c1 = getColumnas();
                cabeceras1 = this.getHeaders();
                matrix = matAux;
                System.out.println("VALORES MATRIX");
                for(int i=0;i<matAux.length;i++){
                    for(int j =0;j<matAux[0].length;j++){
                        System.out.println(matAux[i][j]);
                    }
                }
            }else{
                bandera = false;
            }
            System.out.println("bandera2 "+bandera);
            if(bandera){
                if(generaMatrix(query2)){
                    f2 = getFilas();
                    c2 = getColumnas();
                    cabeceras2 = this.getHeaders();
                    matrix2 = matAux;
                    System.out.println("AQUI ENTRA2");
                }else{
                    bandera=false;
                }
            }
            System.out.println("bandera3 "+bandera);
            System.out.println("AQUI ENTRA");
            if(bandera){
                //validacion de que los campos de tabla1 y tabla2 sean los mismos en nombre y cantidad
                int cuentas1=0;
                if(cabeceras1.size()==cabeceras2.size()){
                    int numero[] = new int[cabeceras1.size()];
                    String mat2[][]= new String[matrix2.length][cabeceras2.size()];
                    System.out.println("SIZE "+cabeceras1.size());
                    for(int i=0;i<cabeceras1.size();i++){
                        for(int j=0;j<cabeceras2.size();j++){
                            if(cabeceras1.get(i).equals(cabeceras2.get(j))){
                                numero[i]=j;
                                cuentas1++;
                                break;
                            }
                        }
                    }
                    if(cuentas1!=cabeceras1.size()) bandera=false;
                    if(bandera){//Ahora a ordenar las tablas para que tengan el mismo formato, para posteriormente hacer el union
                        System.out.println("Entra");
                        //formateando el encabezado
                        /*
                        ArrayList<String> auxS = new ArrayList();
                        for(int i=0;i<cabeceras2.size();i++) auxS.add("null");
                        
                        for(int i=0;i<cabeceras2.size();i++){
                            auxS.add(numero[i], cabeceras2.get(i));
                        }
                        while(auxS.remove("null"));
                        
                        cabeceras2 = auxS;
                        //System.out.println("IMPRIMIENDO CABECERAS CORRECTAMENTE");
                        //for(int i=0;i<cabeceras2.size();i++) System.out.println("Titulo "+cabeceras2.get(i));
                        //Cambiando las columnas de cada tupla para que tengan el mismo formato
                        System.out.println("Tamano matrixAuxiliar "+mat2.length+" , "+mat2[0].length);
                        for(int i=0;i<matrix2.length;i++){
                            for(int j=0;j<matrix2[0].length;j++){
                                System.out.println(numero[j]);
                                mat2[i][numero[j]]=matrix2[i][j];
                            }
                        }
                        System.out.println("SALIDA 1");*/
                        for(int i =0;i<matrix.length;i++){
                            for(int j=0;j<matrix[0].length;j++){
                                System.out.print(matrix[i][j]);
                            }
                            System.out.println("");
                        }
                         System.out.println("Salida 2");
                        for(int i =0;i<matrix2.length;i++){
                            for(int j=0;j<matrix2[0].length;j++){
                                System.out.print(matrix2[i][j]);
                            }
                            System.out.println("");
                        }
                        //HACIENDO LA UNION
                        //Primero se verificara que no existan tuplas repetidas(en caso de estar repetida se pondra en el string el valor "$null")
                        boolean ban=false;
                        for(int i=0;i<matrix.length;i++){
                            for(int j=0;j<matrix2.length;j++){
                                ban=false;
                                if(matrix[i][0].equals(matrix2[j][0])){
                                    ban =true;
                                    for(int k=0;k<matrix[0].length;k++){
                                        if(!matrix[i][k].equals(matrix2[j][k])){
                                            ban = false;
                                        }
                                    }
                                    if(ban){
                                        for(int k=0;k<matrix2[0].length;k++){
                                            matrix2[j][k]="$null";
                                        }
                                    }
                                }
                            }
                        }
                        /*for(int i =0;i<matrix.length;i++){
                            for(int j=0;j<matrix[0].length;j++){
                                for(int k =0;k<matrix2.length;k++){
                                    ban = false;
                                    int f,c;
                                    if(matrix[i][0].equals(matrix2[k][0])){
                                        ban = true;
                                        for(int l=0;l<matrix2[0].length;l++){
                                            if(!matrix[i][j].equals(matrix2[k][l])){
                                                ban=false;
                                            }
                                        }
                                        if(ban){
                                            for(f=0;f<matrix2[0].length;f++){
                                                matrix2[k][f]="$null";
                                            }
                                        }
                                    }
                                }
                            }
                        }*/
                        System.out.println("FORMATO");
                        int contador=0;
                        tuplas2 = new boolean[matrix2.length];
                        for(int i=0;i<matrix2.length;i++) tuplas2[i]=false;
                        for(int i =0;i<matrix2.length;i++){
                            if(!"$null".equals(matrix2[i][0])){
                                contador++;
                                tuplas2[i]=true;
                            }
                        }
                        matAux = new String[matrix.length+contador][matrix[0].length];
                        for(int i=0;i<matrix.length;i++){
                            for(int j=0;j<matrix[0].length;j++){
                                matAux[i][j]=matrix[i][j];
                            }
                        }
                        ArrayList<String> datos = new ArrayList();
                        for(int i=0;i<matrix2.length;i++){
                            for(int j=0;j<matrix2[0].length;j++){
                                if(!"$null".equals(matrix2[i][j])){
                                    datos.add(matrix2[i][j]);
                                }
                            }
                        }
                        for(int i=matrix.length;i<matAux.length;i++){
                            for(int j=0;j<matrix[0].length;j++){
                                matAux[i][j]=datos.remove(0);
                            }
                        }
                        System.out.println("MOSTRANDO NUEVA TABLA");
                        for(int i =0;i<matAux.length;i++){
                            for(int j=0;j<matAux[0].length;j++){
                                System.out.print(matAux[i][j]);
                            }
                            System.out.println("");
                        }
                        
                    }
                }else{
                    bandera=false;
                }
            }
        }else{
            bandera=false;
        }
        return bandera;
    }
    public boolean crossJoinTablas(){
        boolean bandera=true;
        int f1,f2,c1,c2;
        cabeceras1=new ArrayList();
        cabeceras2=new ArrayList();
        if(!"".equals(query2)){
            if(generaMatrix(query1)){
                f1 = getFilas();
                c1 = getColumnas();
                cabeceras1 = this.getHeaders();
                matrix = matAux;
                System.out.println("VALORES MATRIX");
                for(int i=0;i<matAux.length;i++){
                    for(int j =0;j<matAux[0].length;j++){
                        System.out.println(matAux[i][j]);
                    }
                }
            }else{
                bandera = false;
            }
            //System.out.println("bandera2 "+bandera);
            if(bandera){
                if(generaMatrix(query2)){
                    f2 = getFilas();
                    c2 = getColumnas();
                    cabeceras2 = this.getHeaders();
                    matrix2 = matAux;
                    //System.out.println("AQUI ENTRA2");
                }else{
                    bandera=false;
                }
            }
            //System.out.println("bandera3 "+bandera);
            System.out.println("AQUI ENTRA");
            if(bandera){
                //validacion de que los campos de tabla1 y tabla2 sean los mismos en nombre y cantidad
                    if(bandera){//Ahora a ordenar las tablas para que tengan el mismo formato, para posteriormente hacer el union
                        System.out.println("Entra");
                        //formateando el encabezado
                        /*
                        ArrayList<String> auxS = new ArrayList();
                        for(int i=0;i<cabeceras2.size();i++) auxS.add("null");
                        
                        for(int i=0;i<cabeceras2.size();i++){
                            auxS.add(numero[i], cabeceras2.get(i));
                        }
                        while(auxS.remove("null"));
                        
                        cabeceras2 = auxS;
                        //System.out.println("IMPRIMIENDO CABECERAS CORRECTAMENTE");
                        //for(int i=0;i<cabeceras2.size();i++) System.out.println("Titulo "+cabeceras2.get(i));
                        //Cambiando las columnas de cada tupla para que tengan el mismo formato
                        System.out.println("Tamano matrixAuxiliar "+mat2.length+" , "+mat2[0].length);
                        for(int i=0;i<matrix2.length;i++){
                            for(int j=0;j<matrix2[0].length;j++){
                                System.out.println(numero[j]);
                                mat2[i][numero[j]]=matrix2[i][j];
                            }
                        }
                        System.out.println("SALIDA 1");*/
                        for(int i =0;i<matrix.length;i++){
                            for(int j=0;j<matrix[0].length;j++){
                                System.out.print(matrix[i][j]);
                            }
                            System.out.println("");
                        }
                         System.out.println("Salida 2");
                        for(int i =0;i<matrix2.length;i++){
                            for(int j=0;j<matrix2[0].length;j++){
                                System.out.print(matrix2[i][j]);
                            }
                            System.out.println("");
                        }
                        //HACIENDO CROSSJOIN
                        //SE GENERA UNA NUEVA TABLA DE DIMENSION N.FILAS X M.FILAS FILAS Y N+M
                        //SOLO SE MUESTRAN TUPLAS REPETIDAS, LAS DEMAS SE ASIGNARAN CON EL NOMBRE ("$null")
                        /*boolean ban=false;
                        tuplas3 = new boolean[matrix2.length];
                        for(int i=0;i<matrix.length;i++){
                            for(int j=0;j<matrix2.length;j++){
                                ban=false;
                                if(matrix[i][0].equals(matrix2[j][0])){
                                    ban =true;
                                    for(int k=0;k<matrix[0].length;k++){
                                        if(!matrix[i][k].equals(matrix2[j][k])){
                                            ban = false;
                                        }
                                    }
                                    if(ban){
                                        for(int k=0;k<matrix2[0].length;k++){
                                            tuplas3[j]=true;
                                        }
                                    }
                                }
                            }
                        }*/
                        /*for(int i =0;i<matrix.length;i++){
                            for(int j=0;j<matrix[0].length;j++){
                                for(int k =0;k<matrix2.length;k++){
                                    ban = false;
                                    int f,c;
                                    if(matrix[i][0].equals(matrix2[k][0])){
                                        ban = true;
                                        for(int l=0;l<matrix2[0].length;l++){
                                            if(!matrix[i][j].equals(matrix2[k][l])){
                                                ban=false;
                                            }
                                        }
                                        if(ban){
                                            for(f=0;f<matrix2[0].length;f++){
                                                matrix2[k][f]="$null";
                                            }
                                        }
                                    }
                                }
                            }
                        }*/
                        System.out.println("FORMATO");
                        /*int contador=0;
                        for(int i=0;i<tuplas3.length;i++){
                            if(tuplas3[i])contador++;
                        }
                        /*
                        tuplas2 = new boolean[matrix2.length];
                        for(int i=0;i<matrix2.length;i++) tuplas2[i]=false;
                        for(int i =0;i<matrix2.length;i++){
                            if(!"$null".equals(matrix2[i][0])){
                                contador++;
                                tuplas2[i]=true;
                            }
                        }*/
                        matAux = new String[matrix.length*matrix2.length][matrix[0].length+matrix2[0].length];
                        /*
                        for(int i=0;i<matrix.length;i++){
                            for(int j=0;j<matrix[0].length;j++){
                                matAux[i][j]=matrix[i][j];
                            }
                        }*/
                        ArrayList<String> datos = new ArrayList();
                        for(int i=0;i<matrix2.length;i++){
                            for(int j=0;j<matrix.length;j++){
                                for(int k=0;k<matrix[0].length;k++){
                                    //System.out.print("FORMATO"+matrix[j][k]);
                                    datos.add(matrix[j][k]);
                                }
                                System.out.println("");
                            }
                            
                        }
                        System.out.println("Primera tabla");
                        for(int i=0;i<matAux.length;i++){
                            for(int j=0;j<matrix[0].length;j++){
                                matAux[i][j]=datos.remove(0);
                                //System.out.print(matAux[i][j]);
                            }
                            //System.out.println("");
                        }
                        //System.out.println("SEGUNDA tabla");
                        datos = new ArrayList();
                        for(int i=0;i<matrix2.length;i++){
                            for(int j=0;j<matrix.length;j++){
                                for(int k=0;k<matrix2[0].length;k++){
                                    datos.add(matrix2[i][k]);
                                    //System.out.print(matrix2[i][k]);
                                }
                                //System.out.println("");
                            }
                        }
                        for(int i=0;i<matAux.length;i++){
                            for(int j=matrix[0].length;j<matAux[0].length;j++){
                                matAux[i][j]=datos.remove(0);
                            }
                        }
                        //
                        System.out.println("MOSTRANDO NUEVA TABLA");
                        for(int i =0;i<matAux.length;i++){
                            for(int j=0;j<matAux[0].length;j++){
                                System.out.print(matAux[i][j]);
                            }
                            System.out.println("");
                        }
                        
                    }
            }
        }else{
            bandera=false;
        }
        return bandera;
    }
    public boolean intersectaTablas(){
        boolean bandera=true;
        int f1,f2,c1,c2;
        ArrayList<String> cabeceras1=new ArrayList();
        ArrayList<String> cabeceras2=new ArrayList();
        if(!"".equals(query2)){
            if(generaMatrix(query1)){
                f1 = getFilas();
                c1 = getColumnas();
                cabeceras1 = this.getHeaders();
                matrix = matAux;
                System.out.println("VALORES MATRIX");
                for(int i=0;i<matAux.length;i++){
                    for(int j =0;j<matAux[0].length;j++){
                        System.out.println(matAux[i][j]);
                    }
                }
            }else{
                bandera = false;
            }
            System.out.println("bandera2 "+bandera);
            if(bandera){
                if(generaMatrix(query2)){
                    f2 = getFilas();
                    c2 = getColumnas();
                    cabeceras2 = this.getHeaders();
                    matrix2 = matAux;
                    System.out.println("AQUI ENTRA2");
                }else{
                    bandera=false;
                }
            }
            System.out.println("bandera3 "+bandera);
            System.out.println("AQUI ENTRA");
            if(bandera){
                //validacion de que los campos de tabla1 y tabla2 sean los mismos en nombre y cantidad
                int cuentas1=0;
                if(cabeceras1.size()==cabeceras2.size()){
                    int numero[] = new int[cabeceras1.size()];
                    String mat2[][]= new String[matrix2.length][cabeceras2.size()];
                    System.out.println("SIZE "+cabeceras1.size());
                    for(int i=0;i<cabeceras1.size();i++){
                        for(int j=0;j<cabeceras2.size();j++){
                            if(cabeceras1.get(i).equals(cabeceras2.get(j))){
                                numero[i]=j;
                                cuentas1++;
                                break;
                            }
                        }
                    }
                    if(cuentas1!=cabeceras1.size()) bandera=false;
                    if(bandera){//Ahora a ordenar las tablas para que tengan el mismo formato, para posteriormente hacer el union
                        System.out.println("Entra");
                        //formateando el encabezado
                        /*
                        ArrayList<String> auxS = new ArrayList();
                        for(int i=0;i<cabeceras2.size();i++) auxS.add("null");
                        
                        for(int i=0;i<cabeceras2.size();i++){
                            auxS.add(numero[i], cabeceras2.get(i));
                        }
                        while(auxS.remove("null"));
                        
                        cabeceras2 = auxS;
                        //System.out.println("IMPRIMIENDO CABECERAS CORRECTAMENTE");
                        //for(int i=0;i<cabeceras2.size();i++) System.out.println("Titulo "+cabeceras2.get(i));
                        //Cambiando las columnas de cada tupla para que tengan el mismo formato
                        System.out.println("Tamano matrixAuxiliar "+mat2.length+" , "+mat2[0].length);
                        for(int i=0;i<matrix2.length;i++){
                            for(int j=0;j<matrix2[0].length;j++){
                                System.out.println(numero[j]);
                                mat2[i][numero[j]]=matrix2[i][j];
                            }
                        }
                        System.out.println("SALIDA 1");*/
                        for(int i =0;i<matrix.length;i++){
                            for(int j=0;j<matrix[0].length;j++){
                                System.out.print(matrix[i][j]);
                            }
                            System.out.println("");
                        }
                         System.out.println("Salida 2");
                        for(int i =0;i<matrix2.length;i++){
                            for(int j=0;j<matrix2[0].length;j++){
                                System.out.print(matrix2[i][j]);
                            }
                            System.out.println("");
                        }
                        //HACIENDO LA INTERSECCION
                        //SOLO SE MUESTRAN TUPLAS REPETIDAS, LAS DEMAS SE ASIGNARAN CON EL NOMBRE ("$null")
                        boolean ban=false;
                        tuplas3 = new boolean[matrix2.length];
                        for(int i=0;i<matrix.length;i++){
                            for(int j=0;j<matrix2.length;j++){
                                ban=false;
                                if(matrix[i][0].equals(matrix2[j][0])){
                                    ban =true;
                                    for(int k=0;k<matrix[0].length;k++){
                                        if(!matrix[i][k].equals(matrix2[j][k])){
                                            ban = false;
                                        }
                                    }
                                    if(ban){
                                        for(int k=0;k<matrix2[0].length;k++){
                                            tuplas3[j]=true;
                                        }
                                    }
                                }
                            }
                        }
                        /*for(int i =0;i<matrix.length;i++){
                            for(int j=0;j<matrix[0].length;j++){
                                for(int k =0;k<matrix2.length;k++){
                                    ban = false;
                                    int f,c;
                                    if(matrix[i][0].equals(matrix2[k][0])){
                                        ban = true;
                                        for(int l=0;l<matrix2[0].length;l++){
                                            if(!matrix[i][j].equals(matrix2[k][l])){
                                                ban=false;
                                            }
                                        }
                                        if(ban){
                                            for(f=0;f<matrix2[0].length;f++){
                                                matrix2[k][f]="$null";
                                            }
                                        }
                                    }
                                }
                            }
                        }*/
                        System.out.println("FORMATO");
                        int contador=0;
                        for(int i=0;i<tuplas3.length;i++){
                            if(tuplas3[i])contador++;
                        }
                        /*
                        tuplas2 = new boolean[matrix2.length];
                        for(int i=0;i<matrix2.length;i++) tuplas2[i]=false;
                        for(int i =0;i<matrix2.length;i++){
                            if(!"$null".equals(matrix2[i][0])){
                                contador++;
                                tuplas2[i]=true;
                            }
                        }*/
                        matAux = new String[contador][matrix[0].length];
                        /*
                        for(int i=0;i<matrix.length;i++){
                            for(int j=0;j<matrix[0].length;j++){
                                matAux[i][j]=matrix[i][j];
                            }
                        }*/
                        ArrayList<String> datos = new ArrayList();
                        for(int i=0;i<matrix2.length;i++){
                            for(int j=0;j<matrix2[0].length;j++){
                                if(tuplas3[i]){
                                    datos.add(matrix2[i][j]);
                                }
                            }
                        }
                        for(int i=0;i<matAux.length;i++){
                            for(int j=0;j<matrix[0].length;j++){
                                matAux[i][j]=datos.remove(0);
                            }
                        }
                        System.out.println("MOSTRANDO NUEVA TABLA");
                        for(int i =0;i<matAux.length;i++){
                            for(int j=0;j<matAux[0].length;j++){
                                System.out.print(matAux[i][j]);
                            }
                            System.out.println("");
                        }
                        
                    }
                }else{
                    bandera=false;
                }
            }
        }else{
            bandera=false;
        }
        return bandera;
    }
}
