package bd;

import java.util.Hashtable;
import javax.swing.JOptionPane;

/**
 *
 * @author Uriel
 */
public class Analizador {
    String cadena;
    boolean error=false;
    String campoValidacion;
    String valorcampo;
    String campos[];//Almacenar los campos que se requieren de cada select
    String tabla;//El nombre de la tabla
    public Analizador(String cadena){
        this.cadena = cadena.toLowerCase();
    }
    public void descompone(){
        String aux ="";
        if(existePalabra("select")&&existePalabra("from")){
            if(cadena.indexOf("select")==0){
                cadena = cadena.substring(6, cadena.length());
                if(existePalabra("(")&&existePalabra(")")&&
                        cadena.indexOf("(")<cadena.indexOf(")")
                        &&cadena.indexOf(")")<cadena.indexOf("from")){
                    aux = cadena.substring(0, cadena.indexOf("("));
                    aux = aux.trim();
                    if(aux.length()>0){
                        error = true;
                    }
                    else{
                        aux = cadena.substring(cadena.indexOf("("),cadena.indexOf(")")+1);
                        //System.out.println("AUX "+aux);
                        cadena = cadena.substring(cadena.indexOf(")")+1);
                        aux = aux.trim();
                        if(aux.length()>0){
                            guardaNombres(aux);
                            //JOptionPane.showMessageDialog(null,"AUX "+aux);
                            
                        }
                        guardaTabla(cadena);
                        if(cadena.indexOf("where")!=-1&&cadena.indexOf("=")!=-1){
                            cadena = cadena.substring(cadena.indexOf("where"));
                            guardaWhere(cadena);
                        }
                    }
                }
                else{
                    error=true;
                }
            }
            else{
                error=true;
            }
        }
        else{
            error=true;
        }
    }
    public void guardaWhere(String c){
        String aux = c;
        int igual;
        aux = aux.substring(aux.indexOf("where")+5);
        aux = aux.replaceAll("\\s", "");
        igual = aux.indexOf("=");
        campoValidacion = aux.substring(0,aux.indexOf("="));
        System.out.println("Validacion "+ campoValidacion);
        valorcampo = aux.substring(aux.indexOf("=")+1, aux.length()-1);
        System.out.println("Valor "+ valorcampo);
        
    }
    public void guardaTabla(String c){
        String aux = c;
        int i,j;
        i = aux.indexOf("from");
        j = aux.indexOf("where");
        if(j!=-1){
            aux = aux.substring(i+4, j);
            aux = aux.replaceAll("\\s", "");
            cadena = cadena.substring(j);
        }else{
            aux = aux.substring(i+4, aux.length()-1);
            aux = aux.replaceAll("\\s", "");
        }
        JOptionPane.showMessageDialog(null, "AUXILIAR"+aux);
        tabla = aux;
    }
    public void guardaNombres(String nombres){
        String aux = nombres;
        int conta=1;
        int indice,j=1;
        if(aux.contains(",")){
            for(int i=0;i<aux.length();i++){
                if(aux.charAt(i)==',')
                    conta++;
            }
            //System.out.println("contador "+conta);
            campos = new String[conta];
            aux = aux.replace("(", "");
            aux = aux.replace(")", "");
            aux = aux.trim();
            aux = aux.replaceAll("\\s", "");
            aux = aux+",";
            //System.out.println("campos "+conta);
            for(int i=0;i<conta;i++){
                campos[i]=aux.substring(0,aux.indexOf(","));
                aux = aux.substring(aux.indexOf(",")+1, aux.length());
            }
        }else{
           campos = new String[conta];
           campos[0] = aux.substring(aux.indexOf("(")+1, aux.indexOf(")"));
        }
    }
    public boolean existePalabra(String c){
        c = c.toLowerCase();
        boolean flag = false;
        int posicion;
        posicion = cadena.indexOf(c);
        if(posicion!= -1)
            flag = true;
        return flag;
    }
    public void imprimeCampos(){
        for(int i=0; i<campos.length;i++){
            System.out.println(campos[i]);
        }
    }
    public String getNombreTabla(){
        return this.tabla;
    }
    public String[] getStringCampos(){
        return this.campos;
    }
    public String getCampoValidacion(){
        return this.campoValidacion;
    }
    public String getValorCampo(){
        return this.valorcampo;
    }
}
