/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd;

import java.io.DataInputStream;
import java.io.FileInputStream;
import javax.swing.table.TableModel;

/**
 *
 * @author Uriel
 */
public class abreMatrix {
    String path;
    String nombre;
    int filas, columnas;
    String matrix[][];
    String cabecera[];
    TableModel ModeloTabla;
    FileInputStream fis;
    DataInputStream entrada;
    abreMatrix(String path){
        this.nombre = "";
        this.path = path;
        this.fis = null;
        this.entrada = null;
    }
    public void cargaMatrix(){
        nombre = "";
        String strAux = "";
        char c;
        try{
           fis = new FileInputStream(path); 
           entrada = new DataInputStream(fis);
           filas = (int)entrada.readInt();
          columnas = (int)entrada.readInt();
          matrix = new String[filas][columnas];
          cabecera = new String[columnas];
           c = 'a';
           while(c != '\n'){
               c = entrada.readChar();
               System.out.println("C "+c);
               nombre = nombre+c;
           }
           for(int i=0;i<columnas;i++){
               strAux = "";
               while(true){
                        c = entrada.readChar();
                        if(c == '\n' || c == '\0')
                            break;
                        strAux += c;
                    }
                    if(!"".equals(strAux))
                        System.out.println(strAux);
                        cabecera[i]=strAux;
           }
           for(int i=0;i<filas;i++){
               //System.out.println("IIIIIIIIIIIIIIIIIIIIIIII");
               for(int j=0; j<columnas; j++){
                   //System.out.println("JJJJJJJJJJJJJJJJJJJJJJJ");
                   strAux = "";
                    while(true){
                        c = entrada.readChar();
                        if(c == '\n' || c == '\0'){
                            /*if(c == '\n')//corregir
                                System.out.println("SALTO DE LINEA");
                            if(c == '\0')//corregir
                                System.out.println("FIN CADENA");*/
                            break;
                        }
                        strAux += c;
                        //if(c=='\n'){
                            //matrix[i][j] = strAux;
                            //strAux = "";
                            //c = '^';
                        //}
                    }
                    if(!"".equals(strAux))
                        System.out.println(strAux);
                        matrix[i][j]=strAux;
                        //matrix[i][j] = strAux;
               }
           }
           for(int i=0;i<cabecera.length;i++){
               cabecera[i] = cabecera[i].toLowerCase();
           }
           entrada.close();
           fis.close();
        }catch(Exception e){
            System.out.println("ERROR "+ e.getMessage());
        }
    }
    public void escribe(){
        System.out.println(nombre);
        System.out.println("Cabecera");
        for(int i=0;i<columnas;i++){
            System.out.println(cabecera[i]);
        }
        System.out.println("MATRIZ");
        for(int i=0;i<filas;i++){
               for(int j=0; j<columnas; j++){
                   System.out.println("matriz en "+matrix[i][j]);
               }
        }
    }
    public String getNombre(){
        return this.nombre;
    }
    public int getFilas(){
        return this.filas;
    }
    public int getColumnas(){
        return this.columnas;
    }

    /**
     *
     * @return
     */
    public String getValor(int i,int j){
        return matrix[i][j];
    }
    public String[] getHeader(){
        return this.cabecera;
    }
}
