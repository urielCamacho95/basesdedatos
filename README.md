# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
El programa hace pequeñas operaciones propias del algebra relacional, simulando el cómo trabajan los manejadores de base de datos, almacenando las tablas en ficheros binarios y posteriormente aplicando operaciones de "intersección, unión, crossjoin, selección y proyección"
* Version 1.0
* [Learn Markdown](proximamente)

### How do I get set up? ###

* Summary of set up//El programa se ejecuta con Netbeans >=8.1 y java jdk8
* Configuration//No necesita configuracion
* Dependencies//No hay dependencias
* Database configuration//No tiene BD
* How to run tests//Los querys se corren con la sintaxis, para un solo query
select(campos separados por comas y si son todos *) from tabla where campo=valor;
ejemplos:
select(*) from tabla1;
select(*) from tabla1 where id=1;
select(nombre,edad) from tabla1 where id=1;
select(nombre,edad) from tabla1 where id=1; union select(nombre,edad) from tabla2;
select(nombre,edad) from tabla1; union select(nombre,edad) from tabla2 where nombre=uriel;
select(nombre,edad) from tabla1; join select(nombre,edad,apellidoPaterno) from tabla2;

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin//Uriel Camacho Hernandez, Puebla City, Mexico. 2017
* Other community or team contact